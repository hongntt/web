@extends('layouts.layout')
@section('NoiDung')
	<div id="row"> <!-- Row-->
		<div class="col-md-12 box"> <!-- cot 1 -->
			<div id="box5" class="cardbox">
				<div class="card-header">
					<h4 id="h4-tittle">Lịch sử giao dịch</h4>
				</div>
				<div class="card-body">
					<div class="history-table">
						<div class="card-body">
							<div class="table-responsive">
								<div id="w0" class="grid-view">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th><a class="asc" href="/lich-su-giao-dich.html" data-sort="_id">ID</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="status">Trạng thái</a></th>
												<th>Ngày giao dịch</th>
												<th><a href="/lich-su-giao-dich.html" data-sort="product_id">Tên</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="link">Link Video</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="quantity">Số lượng</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="view_start">View ban đầu</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="view_current">View hiện tại</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="view_to_reach">View cần đạt</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="updated_at">Cập nhật</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="ordertotal">Đơn giá</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="is_cancel">Hủy</a></th>
												<th><a href="/lich-su-giao-dich.html" data-sort="cancel_time">Thời gian hủy</a></th>
												<th>Số tiền hoàn lại</th>
												<th>Ghi chú</th>
												<th>Gửi báo lỗi</th>
											</tr>
											<tr id="w0-filters" class="filters">
												<td><span class="bmd-form-group"><input type="text" class="form-control" name="OrderSearch[id]" value=""></span>
												</td>
												<td>
													<select id="ordersearch-status" class="form-control" name="OrderSearch[status]">
														<option value="">----------</option>
														<option value="0" selected="">Đang chờ</option>
														<option value="1">Đang chạy</option>
														<option value="2">Đã hoàn thành</option>
														<option value="3">Báo lỗi</option>
													</select>
												</td>
												<td>&nbsp;</td>
												<td>
													<select id="ordersearch-product_id" class="form-control" name="OrderSearch[product_id]">
														<option value="">----------</option>
														<option value="7">Gói 4000h xem Youtube Server T7 Siêu Rẻ (Video 1 giờ)</option>
														<option value="6">Gói 1000 Subcribe Super Fast X3</option>
														<option value="5">View Youtube Targeted UK</option>
														<option value="4">View Youtube Targeted Hoa Kỳ</option>
														<option value="3">Gói 4000h xem Youtube Server T6 Siêu Rẻ (Video 3 giờ)</option>
														<option value="2">Gói 4000h xem Youtube Server T5 Siêu Rẻ (Video 2 giờ)</option>
														<option value="1">Gói 1000 Subscribe Youtube Nhanh S5</option>
													</select>
												</td>
												<td><span class="bmd-form-group">
													<input type="text" class="form-control" name="OrderSearch[link]" value=""></span>
												</td>
												<td>&nbsp;</td>
												<td><span class="bmd-form-group">
													<input type="text" class="form-control" name="OrderSearch[view_start]" value=""></span>
												</td>
												<td><span class="bmd-form-group">
													<input type="text" class="form-control" name="OrderSearch[view_current]" value=""></span>
												</td>
												<td><span class="bmd-form-group">
													<input type="text" class="form-control" name="OrderSearch[view_to_reach]" value=""></span>
												</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
										</thead>
										<tbody>
											<tr><td colspan="16"><div class="empty">Không tìm thấy kết quả nào.</div></td></tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- End cột 3 -->
	</div><!-- End Row -->
@endsection