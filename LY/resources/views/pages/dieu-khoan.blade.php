@extends('layouts.layout')
@section('NoiDung')
	<div id="row"> <!-- Row-->
		<div class="col-md-16 box"> <!-- cot 1 -->
			<div id="box8" class="cardbox">
				<div class="card-header">
					<h4 id="h4-tittle">Câu hỏi thường gặp</h4>
				</div>
				<div class="card-body">
					<h4>Mua View nên để Video ở chế độ gì?</h4>
					<p>Nên để video ở chế độ CÔNG KHAI hoặc KHÔNG CÔNG KHAI. Tuyệt đối không được để video ở chế độ riêng tư</p><br>
					<h4>Có nên bật Kiếm tiền khi mua View?</h4>
					<p>Ở thời điểm hiện tại. Bạn nên tắt tính năng kiếm tiền khi mua view.</p><br>
					<h4>View nguồn gốc từ quốc gia nào?</h4>
					<p>Views nguồn từ các quốc gia trên thế giới. Không chọn lọc riêng được quốc gia bạn mong muốn. Nếu muốn chọn lọc quốc gia, vui lòng chọn đơn hàng Target quốc gia nếu có.</p><br>
					<h4>Mua View có chết kênh không?</h4>
					<p>Không có bằng chứng về việc mua view của bên mình dẫn tới chết kênh. Hiện tại bên mình đã thử nghiệm và đạt kết quả rất tốt, Mình cũng chạy rất nhiều kênh cho team và không có hiện tượng gì xảy ra cả. Chủ yếu do chất lượng Gmail và kênh của các bạn. Dẫu vậy, thuật toán là do google tạo ra, chính vì thế các bạn cân nhắc trước khi mua. Chết kênh có cả nghìn nguyên nhân và đôi khi chẳng làm gì nó cũng chết. Chết kênh hay các vấn đề trên kênh đều không được bảo hành. Chúng tôi chỉ bảo hành sản phẩm do chúng tôi cung cấp.</p><br>
					<h4>Mua View tại nhom8@yt có lên top không?</h4>
					<p>Views chỉ là một yếu tố phụ hỗ trợ bạn SEO tốt hơn chứ không phải cứ mua views là lên TOP.</p><br>
					<h4>Mua View có ăn đề xuất không?</h4>
					<p>Việc đề xuất hiện tại phụ thuộc vào nhiều yếu tố, chủ đề. Tất nhiên view vẫn là yếu tố quan trọng nhất. Thế nhưng để được đề xuất các bạn cần áp dụng nhiều phương pháp và cân bằng các yếu tố.</p>
				</div>
			</div>
		</div> <!-- End cột 3 -->
	</div><!-- End Row -->
@endsection