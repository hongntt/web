<!DOCTYPE html>
<html lang="vi">
<head>
	<title>Trang Chủ</title>
	<base href="{{ asset('') }}">
	<meta name="description" content="Dịch vụ Youtube">
	<meta name="keywword" content="Youtube, Dich vu Youtube">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<!-- <link rel="stylesheet" type="text/css" href="normalize.css"> -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
</head>	
<body>
	<div id="container"> <!-- nội dung bao bọc cả web. -->
		@include('layouts.sidebar')
		<div id="content"> <!-- nội dung  -->
			@include('layouts.header')
			<div class ="main-content"> <!-- Tiêu đề trong phần content -->
				@yield('NoiDung')
			</div> <!-- End nội dung -->
			@include('layouts.footer')
		</div> <!-- End main content -->
	</div> <!-- End nội dung bao web -->
	<script type="text/javascript" src="{{ asset('/js/youtube.js') }}"></script>
</body>
</html>