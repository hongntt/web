<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('trang-chu', [
	'as'=>'trang-chu',
	'uses'=>'PageController@getIndex'
]);

Route::get('toi-uu-kenh',[
	'as'=>'toiuukenh',
	'uses'=>'PageController@getToiUuKenh'
]);

Route::get('chay-quang-cao',[
	'as'=>'chayquangcao',
	'uses'=>'PageController@getChayQuangCao'
]);

Route::get('san-xuat-noi-dung',[
	'as'=>'sanxuatnoidung',
	'uses'=>'PageController@getSanXuatNoiDung'
]);

Route::get('thong-tin-tai-khoan',[
	'as'=>'thongtintaikhoan',
	'uses'=>'PageController@getThongTinTaiKhoan'
]);
Route::get('lich-su-giao-dich',[
	'as'=>'lichsugiaodich',
	'uses'=>'PageController@getLichSuGiaoDich'
]);
Route::get('thong-bao-moi',[
	'as'=>'thongbaomoi',
	'uses'=>'PageController@getThongBaoMoi'
]);
Route::get('huong-dan-mua-hang',[
	'as'=>'huongdanmuahang',
	'uses'=>'PageController@getHuongDanMuaHang'
]);
Route::get('dieu-khoan',[
	'as'=>'dieukhoan',
	'uses'=>'PageController@getDieuKhoan'
]);
Route::get('lien-he',[
	'as'=>'lienhe',
	'uses'=>'PageController@getLienHe'
]);
Route::get('doi-mat-khau',[
	'as'=>'doimatkhau',
	'uses'=>'PageController@getDoiMatKhau'
]);